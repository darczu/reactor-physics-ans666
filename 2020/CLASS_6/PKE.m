%Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%Example 1/Class 6 - Reactor Physics Tutorials  Rev 21-01-2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXECRCISE - POINT REACTOR KINETICS
%Investigate thermal reactor in steady-state with no external neutron
%sources, in which the neutron generation time is 1e-3 s. Assuming one 
%group of delayed neutrons, determine the subsequent change of reactor power with time. 
%Assume power is proportional to flux and it operates with 
%power equal to 1 MWth (approx zero power state). The reactivity is suddenly made:
%(a) is suddenly made 0.0022 positive. 
%(b) is suddenly made 0.0022 negative. 
%(c) is suddenly made 0.0066 positive. 
%(d) is suddenly made 0.0066 negative. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all;
P= 1.0e6;                   %[W] Core Power    
Sigma_f = 1.8e-1;           %[1/m]  Macroscopic XS for fission
E_f =  3.0903e-011;         %[J/fission] Energy per one fission
LAMBDA = 1e-3;              %[s] neutron generation time
beta = 6.5e-3;              %[-] effective delayed neutron fraction
lambda = 0.08;              %[1/s] decay constant for neutron precursors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inserted reactivities:
rho1 = +0.0022;
rho2 = -0.0022;
rho3 = +0.0066;
rho4 = -0.0066;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Initial conditions:
y_eq = beta*P/(LAMBDA*lambda);    %initial precursors
IC = [P, y_eq ];
 TIME = [0.0, 10.0]; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem (a) 
 rho = rho1;
 a = (rho - beta)/(LAMBDA);
 b = lambda;
 c = beta/LAMBDA;
 d = -lambda;
 [T1, X1] = ode45(@(t,x) RHS_PKE(t,x,a,b,c,d)  ,TIME, IC) ; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem (b) 
 rho = rho2;
 a = (rho - beta)/(LAMBDA);
 [T2, X2] = ode45(@(t,x) RHS_PKE(t,x,a,b,c,d)  ,TIME, IC) ; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem (c)
 rho = rho3;
 a = (rho - beta)/(LAMBDA);
[T3, X3] = ode45(@(t,x) RHS_PKE(t,x,a,b,c,d)  ,TIME, IC) ; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem (d)
  rho = rho4;
 a = (rho - beta)/(LAMBDA);
 [T4, X4] = ode45(@(t,x) RHS_PKE(t,x,a,b,c,d)  ,TIME, IC) ; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOTS
figure(1)
hold on
plot(T1,X1(:,1)./1e6,'-r','LineWidth',2)
plot(T3,X3(:,1)./1e6,'-b','LineWidth',2)
legend('rho= +0.0022','rho= +0.0066');
grid on; box on;
xlabel('Time, [s]'); ylabel('Power, [MWth]');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(2)
hold on
plot(T2,X2(:,1)./1e6,'-r','LineWidth',2)
plot(T4,X4(:,1)./1e6,'-b','LineWidth',2)
legend('rho= -0.0022','rho= -0.0066');
grid on; box on;
xlabel('Time, [s]'); ylabel('Power, [MWth]');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADDITIONAL CONTENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ALL IN ONE  reactivities -1.25$ to 1.5$ beta in one plot
 TIME = [0.0, 1.0];
 %Initial conditions
 y_eq = beta*P/(LAMBDA*lambda);    %initial precursors
 IC = [P, y_eq ];
 
 figure
  hold on;
  i=0; %plot couter - to create a legend
  name=[]; %legend names
 % Solution of many PKE problems at once
for rho=-1.25*beta:0.25*beta:1.5*beta
   i=i+1;
   a = (rho - beta)/(LAMBDA);
   b = lambda;
   c = beta/LAMBDA;
   d = -lambda;
   [T, X] = ode45(@(t,x) RHS_PKE(t,x,a,b,c,d)  ,TIME, IC) ; 
   
   h(i)=plot(T,X(:,1)./P,'-','Color',[rand(),rand(),rand()],'LineWidth',1.5);
   set(gca,'yscale','log') %change to semi-logarythmic plot
   %h(i)=semilogy(T,X(:,1).*100./P,'-','Color',[rand(),rand(),rand()],'LineWidth',1.5);
   name{i}=['rho=', num2str(rho/beta), '$'];
end
grid on; box on;
 legend(h,name);

 xlabel('Time, [s]'); %ylabel('Relative Power, [%]');
 ylabel('Relative Power P/P0, [-]');
 
 hold off  %hold off the figure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

