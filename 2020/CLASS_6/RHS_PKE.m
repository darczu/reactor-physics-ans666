%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Righ Hand Side of the ODE system
function dxdt=RHS_PKE(t,x,a,b,c,d)

dxdt = zeros(2,1);		%Initialization

dxdt(1) = a*x(1) + b*x(2);
dxdt(2) = c*x(1) + d*x(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
