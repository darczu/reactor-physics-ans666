% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Example Class 2 nad 3 - Reactor Physics Tutorials  Rev 06-01-2020 Rev 5
% PWR core based on BEAVRS benchmark: http://crpg.mit.edu/pub/beavrs
% ========================================================================
% ======= CLASS 2  EXAMPLE 1 ============================================
% ========================================================================
clc; close all; clear;  %clean workspace, close all windows, graphs and clear the MATLAB workspace
P_e = 1100;     		%Power el. [MWe]
P_t = 3411;     		%Thermal Power [MWt]
CF = 0.5843;     		%Capacity factor [-]
N_FA = 193;     		%No. of FA per core [-]    
N_FR = 264;     		%No. of FR per FA
t_EOC = 574;    		%End of Cycle [days]
M_HM = 81.79*1e6; 		%Initial Mass of Heavy Metal [g]
H_active = 365.76;  	%Active core length [cm]
enrich=2.3667/100;  	%Fuel enrichment [-]
FApitch = 21.50364; 	%FA pitch [cm]
r_fuel = 0.39218;      	%FR radius [cm]
N_A = 6.022e+23;    	%Avogadro number [atoms/mole]
E_1MeV = 1.602e-13; 	%conversion factor MeV -> J 
E_1fission = 192.9; 	%Recov. Energy per one fission of U-235 [MeV/fission]
barn = 1e-24;       	%1 barn in [cm2]
% =======================================================================
%TASK1 - Net plant efficiency at full power, total fuel mass, fissile
%material mass and average fissile mass per assembly. 
eta = P_e/P_t;                                       %Net efficiency
A_U235 = 235.0; A_U238 = 238.0; A_O = 16.0;          %At. weights
A_U = ( (enrich/A_U235) + ( (1-enrich)/A_U238) )^-1; %At. weight of U   [g/mole]
A_UO2 = 2*A_O + 1*A_U; A_fuel = A_UO2;               %At. weight of UO2 [g/mole]   
M_fuel = (A_UO2/A_U)*M_HM;                           %Fuel mass [g]
M_fissile = enrich*M_HM;                             %FIssile Mass [g]
M_fissile_per_FA = M_fissile/N_FA;  	             %Fissile mass per assembly [g]

% =======================================================================
%TASK2 - Estimate fuel volume, fuel density, core total volume, fuel volume fraction
V_fuel = N_FA*N_FR*H_active*pi*r_fuel^2;            %Fuel Volume [cc]
rho_fuel = M_fuel / V_fuel;                         %density [g/cc]
Core_area = N_FA*FApitch^2;                         %Core area [cm2]
Core_volume = Core_area*H_active;                   %Core volume [cc]
Fuel_frac = V_fuel/Core_volume;                     %Fuel volume fraction in the core

% =======================================================================
%TASK 3 - Average power per assembly and fuel rod, estimate average specific power, Core power density.
P_FA = P_t/N_FA;                        %Av. Power per fuel assembly [MWth]   
P_FR = P_t/(N_FA*N_FR);                 %Av. Power per fuel rod [MWth]
p_density = (P_t/Core_volume)*1e6;      %Av. Power density [MW/m3]
p_density_fuel = p_density/Fuel_frac;    %Av. Fuel power density [MW/m3] 	
p_specific = (P_t/M_HM)*1e6;            %Av Specific Power [MW/tHM] or [W/g]

% =======================================================================
%TASK4 - Estimate energy per one fission, fission reaction rate and neutron flux.
E_f = E_1fission*E_1MeV;                %Energy in [J] per one fission event
%Reaction Rate [1/cc/s]=flux*Macro XS [1/cm]=flux*micro XS*Atomic density [at/cc]
%Power density = Reaction Rate * Energy per one fission
R_f = p_density_fuel/E_f;               %Fission Reaction Rate in the fuel   [fissions/cc/s]
N_UO2 = rho_fuel*N_A/A_fuel;            %Atomic mass number for fuel [atoms/cc]
N_U = N_UO2;                            %Atomic mass number for Uranium [atoms/cc]
w_U235 = enrich;  w_U238 = 1 - enrich;  %mass fraction of U235 and U238 
N_U235 = w_U235*N_U*(A_U/A_U235);		%Atomic density of U-235 [atoms/cc]
N_U238 = w_U238*N_U*(A_U/A_U238);		%Atomic density of U-238 [atoms/cc]

sigma_f = 505*1e-24;					%Microscopic cross-section for U235 average for typical thermal spectrum
phi = R_f/(sigma_f*N_U235);				%Average neutron flux in the fuel [neutrons/s/cm2]

% =======================================================================
%TASK 5 - Effective full power length of the cycle. Average fuel burnup at %the End-of-Cycle (EOC) in [MWd/kgHM].
EFPD = t_EOC*CF;                        %Effective Full Power Days [EFPDs/days]
Burnup = (P_t/1e3)*EFPD/(M_HM/1e6);     %Fuel Burnup [GWd/tHM]
Burnup1 = Burnup*1000;                  %[MWd/kgHM]


 
% =======================================================================
% -------- CLASS 3 EXCERCISE 2 ---------------
% =======================================================================

% =======================================================================
% TASK 1 - Estimate mass of the fission products created due to U-235 fission during the irradiation. 
% For simplicity omit the loss of mass due to the mass-energy equivalence. 
% Atomic number density of U235 at the end of cycle
% dN_U235/dt = -sigma_f * N_U235 * flux  - solve with sep. of variables
% N_U235(t) = N_U235(0)*exp(-sigma_f*flux * t);
% =======================================================================

% U-235 concentration @EOC
 N_U235_EOC = N_U235*exp(-sigma_f*phi*EFPD*24*3600);

% Initial mass of U-235
M_U235_0 = M_fissile;

% EOC mass of U-235
M_U235_EOC = M_U235_0*exp(-sigma_f*phi*EFPD*24*3600);

%Approximate mass of FPs @EOC
%Mass of fission products @EOC - neglecting mass transformed into energy accoding to E=mc2 etc....
 M_FP = M_U235_0 - M_U235_EOC ;
 
barn = 1e-24;
% =======================================================================
% TASK 2 - BURNUP - BATEMAN EQUATIONS FOR PU-239
% =======================================================================
%Micro XSs for Pu239 and U238
sPu239f = 698*barn;		
sPu239a = 973*barn;
sPu239c =  sPu239a - sPu239f;
sU238a = 2.42*barn;
sU238c =  sU238a;
time = EFPD*24*3600;

% Transmutation matrix
a=-sU238a*phi; 
b=-a;
c= phi*(-sPu239f-sPu239c);
A = [a, 0  ;
     b, c  ];
N_0 = [N_U238 ; 0.0];	 

%Solution of Bateman Eq. in the form of 
% dN/dt = A*N, where N is column vector N =[N_U238; N_Pu239] and A 2x2 transmutation
% matrix A = [a, 0 ;b,c]
% phi - neutron flux
% Matrix exponential method
N_EOC = expm(A*time)*N_0;

% =======================================================================
% PLOT TIME HISTORY OF Pu-239
% =======================================================================
time2 = linspace(0,time,100);       %time vector with 100 equally spaced time points
N =[];								 %create exmpty vector
for i=1:100						 %loop for 100 time points
   N_end = expm(A*time2(i))*N_0;      %Calculate solution for i-th time point, there is matrix multiplification! 
   N = [N, N_end]; 					 %add solution for i-th time  point to vector collecting results.
end
[time2', N'];     				  %solution matrix  with two columns [time, concetration]




% =======================================================================
% PLOT
% =======================================================================

fig=figure(1);								%figure initation
ylabel('At. Dens. [at/cc]');				%label
xlabel('Time, [days]');						%label
semilogy(time2./(24*3600),N,'.-','LineWidth',2);  %semilogarithmic plot of time and conctrations
grid on										%grid
legend('U-238','Pu-239');					
print(fig,  'Fig1', '-dpng');				%save the figure as PNG file

%It is possible to create similar code with ODE45 function for ODEs.
%It is possible to create code for much larger problems. 



% =======================================================================
% TASK 3 Estimate concentrations U-238, Pu-239, Pu-240 and Pu-241 @EOC. Plot time history. 
% Micro XSs for Pu239 and U238
sPu239c =  sPu239a - sPu239f;
sU238a = 2.42*barn;
sU238c =  sU238a;
sPu239f = 741.0e-24;  %fission
sPu239c = 274.0e-24;  %radiative capture  
sU238c  = 2.73e-24;   %rad. capt.
sPu240c = 263e-24;	  %rad. capt.
sPu241f = 946e-24;	  %fission
sPu241a = 1273e-24;   %abs
sPu241c = (sPu241a-sPu241f); %rad. capt.
time = 24*3600*EFPD;  %time in [s]
lambdaPu241 = log(2)/(14.35*365*24*3600);    %Decay const. = ln(2)/(half-life) [1/s]

a = sU238a*phi;                
b = phi*(sPu239f + sPu239c);
c = phi*(sPu239f);
d = phi*(sPu240c);
e = lambdaPu241 + phi*(sPu241f+sPu241c);

%Transmutation matrix 
A=[-a,  0,  0,  0 ; 
	a, -b,  0,  0 ;
	0,	c, -d,  0 ;
	0,	0,  d, -e ];  
 %Initial conditions
N_0 = [N_U238; 0.0; 0.0; 0.0]; 
 %Application of Matrix Exponential Method
N_end = expm(A*time)*N_0; 

% =======================================================================
% CALCULATE TIME HISTORY
% =======================================================================
time2= linspace(0,time,100);       %time vector with 100 equally spaced time points
N =[];
for i=1:100
   N_end = expm(A*time2(i))*N_0;   %there is matrix multiplification!
   N = [N, N_end]; 
end

[time2', N'];       %solution matrix


% =======================================================================
% PLOT 
% =======================================================================
fig=figure(3);
ylabel('At. Dens. [at/cc]');
xlabel('Time, [days]');
semilogy(time2./(24*3600),N,'.-','LineWidth',2);
grid on
legend('U-238','Pu-239','Pu-240','Pu-241');
print(fig,  'Fig3', '-dpng');				%save the figure as PNG file
























