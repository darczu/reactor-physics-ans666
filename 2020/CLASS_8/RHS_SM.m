%RHS  for Samarium

function dy = RHS_SM(t,y,phi, time, y_Pm, Sigma_f, lambda_Pm, sigma_aSm )
% 1 - Promethium
% 2 - Samarium



%Change conditions for different moments!
if(t >= time(3))
    
    a = y_Pm*Sigma_f*phi(4);
    b = lambda_Pm;
    c = sigma_aSm*phi(4);
    
elseif(t >= time(2))
    a = y_Pm*Sigma_f*phi(3);
    b = lambda_Pm;
    c = sigma_aSm*phi(3);
    
elseif(t >= time(1))    
    a = y_Pm*Sigma_f*phi(2);
    b = lambda_Pm;
    c = sigma_aSm*phi(2);
    
else  %time t<20days
    a = y_Pm*Sigma_f*phi(1);
    b = lambda_Pm;
    c = sigma_aSm*phi(1);
end


%Our equations
dy = zeros(2,1);
dy(1) = a - b*y(1);
dy(2) = b*y(1) - c*y(2);