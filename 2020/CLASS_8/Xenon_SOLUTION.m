%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%Class 8 - Reactor Physics Tutorials  Rev 20-01-2019 - XENON AND SAMARIUM TRANSIENTS
clear; clc; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXERCISE 1 - FRESH CORE START-UP, XENON BULIDUP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%A thermal reactor was loaded with fresh fuel and started. 
%Assume that the thermal neutron flux step chaged from 0 to 2*10e18 [1/m2/s] at t = 0
%lot the concentration of iodine and xenon as a function of time and
%calculate the equlibrium concentrations of the two isotopes in the reactor. Given data:
Sigma_f = 1.8e-1;           %[1/m]  Macroscopic XS for fission
lambda_I = 2.9e-5;          %[1/s]  Decay constant for Iodine    
lambda_X = 2.1e-5;          %[1/s]  Decay constant for Xenon
y_I = 0.061;                %[-]    Fission yield for Iodine
y_X = 0.003;                %[-]    Fission yield for Xenon
sigma_aX = 2.6e6*1e-28;     %[barn = 1e-28 m2]!  Micro XS for Xe absorbtion   
phi0 = 2e+18;                 %[1/s/m2] neutron flux  it is 10^14 n/cm2/s

% SOLUTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Analytical Solution by application of the so called integration factor...
%We will solve it numerically. Matlab or equivalent environments have method to solve ODEs and systems of ODEs.  Our problem is as set of two linear ODEs.
%To solve it we can use for example method called ODE45 - standard matlab ODEs solver.
phi = phi0;                 %[1/s/m2] neutron flux

%Eq. Conc of Xenon and Iodine 
I_eq = phi*(y_I)*Sigma_f/lambda_I
Xe_eq = phi*(y_I + y_X)*Sigma_f/(lambda_X+phi*sigma_aX)
% dI/dt = a*I + b*Xe + e
% dX/dt = c*I + d*Xe + f
a = -lambda_I;
b = 0.0;
c = lambda_I;
d = -sigma_aX*phi - lambda_X;
e = y_I*phi*Sigma_f;
f = phi*Sigma_f*y_X;

TIME = [0, 100*3600];   %Time scale for our solution
IC = [0.0 , 0.0];       %Initial Conditions 
[T, Y] = ode45( @(t,y)RHS(t,y,a,b,c,d,e,f) , TIME, IC);  %Solution
% PLOT
figure(1)
plot(T./3600,Y,'LineWidth',2); grid on;
xlabel('Time [h]'); ylabel('Conc. [at/m3]'); legend('I-135','Xe-135');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXERCISE 2 - LONG TERM REACTOR OPERATION AND SHUTDOWN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The reactor from Excercise 1 was operating a long time at steady state
%conditions and then suddenly shut-dwon.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (a) Plot the concentration of Iodine and Xenon as a function of time after shutdown.
I_eq = phi*(y_I)*Sigma_f/lambda_I;
Xe_eq = phi*(y_I + y_X)*Sigma_f/(lambda_X+phi*sigma_aX);
phi = 0.0   %zero power
IC   = [I_eq,  Xe_eq];    %Initial Conditions 
% dI/dt = a*I + b*Xe + e
% dX/dt = c*I + d*Xe + f
a = -lambda_I;
b = 0.0;
c = lambda_I;
d = -sigma_aX*phi - lambda_X;
e = y_I*phi*Sigma_f;
f = phi*Sigma_f*y_X;

TIME = [0, 100*3600];   %time scale
[T, Y] = ode45( @(t,y)RHS(t,y,a,b,c,d,e,f) , TIME, IC);

figure(2)
plot(T./3600,Y,'LineWidth',2); grid on;
xlabel('Time [h]'); ylabel('Conc. [at/m3]'); legend('I-135','Xe-135');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (b) find maximum concentration of Xenon after shutdown  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Xenon_max, m]=max(Y(:,2))
Time_Xe_max = T(m)/3600
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (c) plot concentration for different fluxes before shutdown
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Flux before shutdown
phi1 = phi0.*[0.05, 0.1, 0.5, 1.0, 2.0];

TIME = [0, 100*3600];   %time scale

%PLOT WITH SOLUTION
figure(3)
phi1 = phi0.*[0.001, 0.01, 0.05, 0.1, 0.5, 1.0, 2.0, 5.0, 10.0]
map=colormap(hsv(numel(phi1)));

for i =1:numel(phi1)
    phi = phi1(i);  %new flux
    % Recalculation Equilibrium state for different flux
    I_eq = phi*(y_I)*Sigma_f/lambda_I;   %new I eq. concentr
    Xe_eq = phi*(y_I + y_X)*Sigma_f/(lambda_X+phi*sigma_aX);  %new eq. concentr
    IC   = [I_eq,  Xe_eq];
    phi =0.0;  %shutdown
    a = -lambda_I;
    b = 0.0;
    c = lambda_I;
    d = -sigma_aX*phi - lambda_X;
    e = y_I*phi*Sigma_f;
    f = phi*Sigma_f*y_X;
    
    % dI/dt = a*I + b*Xe + e
    % dX/dt = c*I + d*Xe + f
    [T, Y] = ode45( @(t,y)RHS(t,y,a,b,c,d,e,f) , TIME, IC);
    %plot(T/3600, Y(:,2)./Xe_eq,'Color',[rand(),rand(),rand()],'LineWidth',2);
    semilogy(T/3600, Y(:,2)./Xe_eq,'Color',map(i,:),'LineWidth',2);
    Xe_eq
    
    %rho = sigma_aX.*(y_I + y_X).*Sigma_f.*Y(:,2);
    
    %plot(T/3600, rho,'Color',[rand(),rand(),rand()],'LineWidth',2);
    hold on
    
end
%title('Xe-135 reactivity for different fluxes before shutdown');
title('Xe-135 concentration for different fluxes before shutdown, base flux 1e+14 [n/cm2/s]');
xlabel('Time [h]'); ylabel('Xe-135 Conct. / Eq. Xe-135 Conct. '); legend('0.001', '0.01', '0.05', '0.1','0.5','1.0','2.0', '5.0','10.0'); grid on;
%xlabel('Time [h]'); ylabel('Ractivity. [dk/k]'); legend('0.1','0.5','1.0','2.0','5.0'); grid on;


% REACTIVITY IMPACT
figure(4)
phi2 = linspace(eps,1e19,1001);
Xe_eq_ref = 1e18.*(y_I + y_X).*Sigma_f./(lambda_X+1e18.*sigma_aX);
Xe_eq_1 = phi2.*(y_I + y_X).*Sigma_f./(lambda_X+phi2.*sigma_aX);
plot(phi2./1e18, Xe_eq_1./Xe_eq_ref,'Color','r','LineWidth',2);
title('Xe-135 equilibrium conctration vs flux')
xlabel('Neutron Flux/1e+14 [1/cm2/s], [-]');
ylabel('Xe-135 Conct. / Xe-135 Conct. for 1e+14 [1/cm2/s], [-]');
grid on;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXERCISE 3  - POWER REDUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The reactor from Ex.1 was operating a long time at stationary conditions
%and suddenly a partial scram with 50% of power was introduced
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (a) Plot the concentration of Iodine and Xenon as a function of time after partial scram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
phi = phi0;
I_eq = phi*(y_I)*Sigma_f/lambda_I;
Xe_eq = phi*(y_I + y_X)*Sigma_f/(lambda_X+phi*sigma_aX);

phi =  0.5*phi0;            %half power
IC   = [I_eq,  Xe_eq];    %Initial Conditions 

    % dI/dt = a*I + b*Xe + e
    % dX/dt = c*I + d*Xe + f
    a = -lambda_I;
    b = 0.0;
    c = lambda_I;
    d = -sigma_aX*phi - lambda_X;
    e = y_I*phi*Sigma_f;
    f = phi*Sigma_f*y_X;
TIME = [0, 100*3600];   %time scale
[T, Y] = ode45( @(t,y)RHS(t,y,a,b,c,d,e,f) , TIME, IC);

figure(5)
plot(T./3600,Y,'LineWidth',2);
xlabel('Time [h]'); ylabel('Conc. [at/m3]'); legend('I-135','Xe-135');grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (b)  find maximum concentration of Xenon after shutdown and time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Xenon_max2, m]=max(Y(:,2))
Time_Xe_max2 = T(m)/3600
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% (c) Plot the Xe-135 concentration for neutron flux changed to: 5%, 10%, 50%, 100%, 200% of nominal value.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The same initial conditions
IC   = [I_eq,  Xe_eq];    %Initial Conditions 
TIME = [0, 100*3600];     %Time scale

figure(6)
phi1 = phi0.*[0.05, 0.1, 0.5, 1.0, 2.0];
for i =1:numel(phi1)
    phi = phi1(i);  %new flux
    a = -lambda_I;
    b = 0.0;
    c = lambda_I;
    d = -sigma_aX*phi - lambda_X;
    e = y_I*phi*Sigma_f;
    f = phi*Sigma_f*y_X;
    [T, Y] = ode45( @(t,y)RHS(t,y,a,b,c,d,e,f) , TIME, IC);
    plot(T/3600, Y(:,2),'Color',[rand(),rand(),rand()],'LineWidth',2);
    hold on
end
title('Plot the Xe-135 concentration for neutron flux changed to');
xlabel('Time [h]'); ylabel('Conc. [at/m3]'); legend('5%','10%','50%','100%','200%'); grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SAMARIUM - EXERCISE 1+2+3 ver for Samarium 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Startup -> 20days shutdown -> 15 days -> restart to 100%-> 25 days ->
% power reduction to 50% -> 65 days
%Same reactor and same problems - but investigate Samarium
sigma_aSm = (6.15e4)*1e-28;
lambda_Pm = log(2)/(53.1*3600); 
y_Pm = 1.13e-2; 
phi0 = 2.0e+18;                 %[1/s/m2] neutron flux  it is 10^14 n/cm2/s
Sigma_f = 1.8e-1;
tic %timer to count computational time

phi1 = phi0;            % 0-30   days  initial flux
phi2 = 0.0;             % 30-60  days    
phi3 = phi0;            % 60-90  days
phi4 = phi0*0.5;        % 90-120 days
phi = [phi1, phi2, phi3, phi4]; %Vector with fluxes and time moments for flux change

time1 = 30*24*3600;
time2 = 60*24*3600;
time3 = 90*24*3600;
time4 = 160*24*3600; %end time
time = [time1, time2, time3, time4];

%Equlibrium concentrations at beginig:
Pm_eq = y_Pm*Sigma_f*phi(1) /lambda_Pm
Sm_eq = y_Pm*Sigma_f/(sigma_aSm)

%Our equations
% dPm/dt = y_Pm*Sigma_f*phi - lambda_Pm*Pm =  a - b*Pm
% dSm/dt = lambda_Pm*Pm - sigma_aSm*phi*Sm = b*Pm - c*Sm

TIME = [0, time4];   %Time scale for our solution
IC = [0.0 , 0.0];   %Initial Conditions - clean core

%Solution - we have to send more information to our function
[T, Y] = ode15s( @(t,y)RHS_SM(t,y,phi,time,y_Pm, Sigma_f, lambda_Pm, sigma_aSm ) , TIME, IC);

figure;
plot(T./(3600*24),Y(:,2),'-r','LineWidth',2); grid on;
xlabel('Time [days]'); ylabel('Conc. [at/m3]'); 
hold on;
plot(T./(3600*24),Y(:,1),'-b','LineWidth',2); grid on;
xlabel('Time [days]'); ylabel('Conc. [at/m3]'); legend('Sm-149','Pm-149');
title ('Samarium and Promethium history. Start-up -> Shutdown ->Start ->Power reduction 50%')
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

