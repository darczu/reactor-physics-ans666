%RHS
%Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%E Class 4 - Reactor Physics Tutorials  Rev 08-01-2017

function dy = RHS(t,y,a,b,c,d,e,f)

dy = zeros(2,1);
dy(1) = a*y(1)+b*y(2)+e;
dy(2) = c*y(1)+d*y(2)+f;
