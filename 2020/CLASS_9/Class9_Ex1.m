%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 5 Examples - Reactor Physics Tutorials  Rev 05-02-2019
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% CLASS 5 EXERCISE 1 - SLAB SPATIAL DIFFUSION WITH CONSTANT SOURCE 
% Solve one speed neutron diffusion equation in 1D slab geometry. Assume symmetry - no flux boundary condition on the left side of the slab and zero flux condition on the right side.
% Solve problem for constant neutron source equal to: 3.0e+14 n/s/cc
% Medium is non-multiplying
% Core data are based on PWR core discussed during Class 2.
% Slab half-thincknes:    H = 180 cm
% Diffusion coeffcient:   D = 9.2106 cm
% Macroscopic abs. XS: Sigma_a  0.1532 1/cm
clear; clc; close all;  
H = 200;           %slab thickness
a = H/2;           %half-thickness
D = 9.2106;        %diffusion coeffcient
Sigma_a =  0.1532; %Abs. macro XS
N = 100;           %nodalization number of divisions
nodes = N+1;       %number of nodes
h = H/N;           %constant node interval - step
Source = 3e14;     %constant source [n/cc/s]
L = sqrt(D/Sigma_a);  %Diffusion length
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SOLUTION - NUMERICAL
phi = NeutronSolver_Ex1(N,D,h,Sigma_a,Source);
x = linspace(-a,a,nodes);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SOLUTION - ANALITICAL FOR SLAB
phiA = (1- (cosh(x./L)./cosh(a./L))).*(Source/Sigma_a); %plane
phiA = phiA';
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PLOT
figure(1)
hold on;
plot(x,phi,'or','LineWidth',2)
plot(x,phiA,'-b','LineWidth',2)
title('Flux distribution');
ylabel('Flux, [n/cm2/s]')
xlabel('Distance, [cm]');
grid on; box on;
figure(2)
rel_error = 100*abs((phi-phiA)./phiA);
plot(x,rel_error,'-om','LineWidth',2)
title('Relative error dstribution');
ylabel('Relative Error, [%]');
xlabel('Distance, [cm]');
