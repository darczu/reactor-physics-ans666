%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%Example Class 7 - Reactor Physics Tutorials  Rev 22-01-2019
% Separated function to calculate reactivity explicitly
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rho= IN_REACTIVITY(t,x,data,opt,opt1)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        1    2      3    4    5    6   7   8    9   10   11  12  13  14  15   16
%data: beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0
beta= data(1);
r_f = data(13);
r_c = data(14);
w_c = data(10);
t0  = data(4);
rho0 = data(5);
T_c0 = data(15);
T_f0 = data(16);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(t < t0)   %initial time
	rho_c = 0;    
elseif(t >=t0) %transient initation time

	if(opt == 'a')
	%(a) Hot Full Power (HFP) rapid control rod ejection. Rod worth is 0.2$,0.4$,0.6$,0.8$,1$,1.2$.  
	rho_c = opt1*beta; 

	elseif(opt == 'b')
	%(b) Hot Zero Power (HZP) rapid control rod ejection. Rod worth  0.2$,0.4$,0.6$,0.8$,1$,1.2$.     (P=10 MWth)
	rho_c = opt1*beta;

	elseif(opt == 'c')
	%(c) Ramp control rod removal at Hot Full Power. Worth 0.5$, time of rod removal 60s. 
	  if ((t-t0) < 60)
		rho_c = opt1*beta*((t-t0)/60);
	  else
		rho_c = opt1*beta;
	  end
	  
	elseif(opt == 'd')
	%(d) SCRAM with -0.5$, -1.0$ and -5.0$ at Hot Full Power
	  rho_c = opt1*beta; 

	elseif(opt == 'e')
	%(e) Cold water injection at Hot Full Power T_cin=530K 
	   %T_cin = 545.0;
	   rho_c = 0.0;
	    
	elseif(opt == 'f')
	%(f) One reactor coolant pump trip (reactor has 4 of them). Reactor coolant pump coast-down time is 60s. Initial State: HFP.
	   %w_c = ((4-opt1)/4)*w_c + (opt1/4)*w_c*exp(-(t-t0)/60.0);   
       rho_c = 0.0;
	end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rho = rho0 + rho_c + r_f*(x(3)-T_f0) + r_c*(x(4)-T_c0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
