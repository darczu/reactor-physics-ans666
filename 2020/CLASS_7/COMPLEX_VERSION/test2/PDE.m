%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
%Example Class 7 - Reactor Physics Tutorials  Rev 18-01-2020
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXECRCISE - CLASS 7 - POINT REACTOR DYNAMICS
% Example based on Lecture 10 example
% Transients for Light Water Reactor. Use point neutron dynamics with 
% one neutron precursor group approximation to simulate some events/transients. 
clear; close all; clc;  format short g;  tic  %time counter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%(a) Hot Full Power (HFP) rapid control rod ejection. Rod worth is 0.2$,0.4$,0.6$,0.8$,1$,1.2$.  
%(b) Hot Zero Power (HZP) rapid control rod ejection. Rod worth  0.2$,0.4$,0.6$,0.8$,1$,1.2$.     (P=10 MWth)
%(c) Ramp control rod removal at Hot Full Power. Worth 0.5$, time of rod removal 60s. 
%(d) SCRAM with -0.5$, -1$ and -5$ at Hot Full Power
%(e) Cold water injection at Hot Full Power T_cin=530K 
%(f) One reactor coolant pump trip (reactor has 4 of them). Reactor coolant pump coast-down time is 60s. Initial State: HFP.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
option = 'a';  %TRANSIENT SELECTION  'TANSIENT LETTER' : a,b,c,d,e,f  <========= SELECT TRANSIENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reactor data:
LAMBDA = 1.0e-3;            %[s]    Neutron generation time
beta = 7.5e-3;              %[-]    Effective delayer neutron fraction
lambda = 0.08;              %[1/s]  Decay constant for neutron precursors
cp_c = 4000.0;              %[J/K/kg] spec. heat capacity for coolant
cp_f = 200.0;               %[J/kg/K] specific heat capacity for fuel 
m_f = 90000.0;              %[kg]   Fuel mass
m_c = 15000.0;              %[kg]   Coolant mass
w_c = 17000.0;              %[kg/s] Coolant flow
T_cin = 550.0;              %[K]    Coolant inlet temperature
hA = 10.0e6;                %[W/K]  Heat Transfer Coefficient x Heat Transfer Area
r_f = -1.0e-5;              %[1/K]  Doppler/Prompt Reactivity Feedback Coefficient 
r_c = -5.0e-5;              %[1/K]  Moderator/Delayed Reactivity Feedback Coefficient

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INITIAL CONDITIONS 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Power0 = 3411.0e6; 			%[Wth]  Nominal Power
rho0 = 0.0;                  %Critical core - initial reactivity

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TIME SELECTION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t0 = 10.0;                   %[s] - transient initiation time 
TIME = [0.0, 60.0];         %[s] - initial and final time - time span

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%option1 = value of the parameter for case under investigation
if(option == 'a')      %(a) Hot Full Power (HFP) rapid control rod ejection. Rod worth is 0.2$,0.4$,0.6$,0.8$,1$,1.2$.  
   option1 = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2]; 
   param = '$';
elseif(option == 'b')  %(b) Hot Zero Power (HZP) rapid control rod ejection. Rod worth  0.2$,0.4$,0.6$,0.8$,1$,1.2$.     (P=10 MWth)
   Power0 = 10.0e6;               %[Wth] Hot Zero Power
   option1 = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2]; 
   param = '$';
elseif(option == 'c')  %(c) Ramp control rod removal at Hot Full Power. Worth 0.5$, time of rod removal 60s. 
    option1 = [0.25,0.5,0.75,1.0, 1.25];    
	param = '$';
elseif(option == 'd')  %(d) SCRAM with -0.5$, -1$ and -5$ at Hot Full Power
    option1 = [-0.25, -0.5, -1.0, -5.0, -10.0];
	param = '$';
elseif(option == 'e')  %(e) Cold water injection at Hot Full Power T_cin=500,525,550, 575K 
    option1 = [500, 525, 550, 575, 600];
	param = 'K';
elseif(option == 'f')  %(f) One reactor coolant pump trip (reactor has 4 of them). Reactor coolant pump coast-down time is 60s. Initial State: HFP.
    option1 = [1,2,3,4];
	param = ' pump(s) trip';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EQUILIBRIUM CONDITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
y_eq = beta*Power0/(LAMBDA*lambda);    %Precursors concentration at steady-state
T_c0 = T_cin + Power0/(2*w_c*cp_c);    %Steady-State coolant average temperature
T_f0 = T_c0 + Power0/(hA);             %Steady-State coolant average temperature
IC = [Power0, y_eq, T_f0, T_c0];       %Initial conditions vector

%% STORE DATA IN ONE VECTOR
%        1    2      3    4    5    6   7    8   9   10   11  12  13  14  15   16
%data: beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0
DATA = [beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0]; %Input DATA in one vector

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SOLUTION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LOOP FOR ALL OPTIONS - CALCULATE AND COMPARE AT ONCE

color_hsv = hsv(numel(option1))  %Different coloring based on HSV.

for(j=1:numel(option1))

	[T X] = ode23s(@(t,x) RHS_PDE_EXPLICIT(t,x,DATA,option,option1(j)), TIME, IC );
	% RE-CALCULATION OF THE REACTIVITY - relation for the reactivity is given explicitly so we can simply recalculate it with solution

	reactivity = [];
	for i=1:numel(T)
	 reactivity(i) = IN_REACTIVITY(T(i),X(i,:),DATA,option,option1(j));
	end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PLOTS 
    %color = [rand(),rand(),rand()]; % Random rolors
	color = color_hsv(j,:); %hsv coloring
	fig1 = figure(1)
	h1(j)=plot(T,X(:,1)./Power0,'Color',color,'LineWidth',2); 
	xlabel('Time, [s]'); ylabel('Power/Power0, [-]'); grid on; hold on; box on;
    
	
	fig2 = figure(2)
	h2(j)=plot(T,X(:,3),'Color',color,'LineWidth',2); 
	ylabel('Fuel Temperature, [K]'); grid on; hold on; box on;
   
	
	fig3 = figure(3)
	h3(j)=plot(T,X(:,4),'Color',color,'LineWidth',2); 
	xlabel('Time, [s]'); ylabel('Coolant Temperature, [K]'); grid on; hold on; box on;
   
	fig4 = figure(4)
	h4(j)=plot(T,reactivity/beta,'Color',color,'LineWidth',2); 
	xlabel('Time, [s]'); ylabel('Reactivity, [$]'); grid on; hold on; box on;
    
    fig5 = figure(5)
	h5(j)=plot(T,X(:,1)./1e6,'Color',color,'LineWidth',2); 
	xlabel('Time, [s]'); ylabel('Power, [MWth]'); grid on; hold on; box on;
	
	fig6 = figure(6)
	%h6(j)=plot(T,X(:,2),'Color',color,'LineWidth',2); 
	%xlabel('Time, [s]'); ylabel('Precursors Concentration, [-]'); grid on; hold on; box on;
	h6(j)=plot(T,X(:,2)./y_eq,'Color',color,'LineWidth',2); 
	xlabel('Time, [s]'); ylabel('Relative Precursors Concentration (y/y_{eq}), [-]'); grid on; hold on; box on;
	
	name{j} = [ num2str(option1(j)), ' ', param ];  %to be usd in legend as a case name
	
end  %END OF LOOP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Plot legends - method to make variable legends
legend(h1,name); 
legend(h2,name);
legend(h3,name);
legend(h4,name);
legend(h5,name);
legend(h6,name);

print(fig1,  ['Fig1', option], '-dpng');				%save the figure as PNG file
print(fig2,  ['Fig2', option], '-dpng');				%save the figure as PNG file
print(fig3,  ['Fig3', option], '-dpng');				%save the figure as PNG file
print(fig4,  ['Fig4', option], '-dpng');				%save the figure as PNG file
print(fig5,  ['Fig5', option], '-dpng');				%save the figure as PNG file
print(fig6,  ['Fig6', option], '-dpng');				%save the figure as PNG file


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% @ - it is handle function
% ODE45 https://www.mathworks.com/help/matlab/ref/ode45.html  - method for non-stiff ODEs
% ODE23s https://www.mathworks.com/help/matlab/ref/ode23s.html?searchHighlight=ode23s&s_tid=doc_srchtitle   - method for stiff ODEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%METHOD TO TRANSFER VARIABLES FROM FUNCTION TO THE WORKSPACE (I WAS NOT USED HERE IT ANT IT IS NOT USEFUL FOR REACTIVITY):
% https://www.mathworks.com/matlabcentral/answers/92701-how-do-i-pass-out-extra-parameters-using-ode23-or-ode45-from-the-matlab-ode-suite
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%RHO - REACITIVTY CAN BE RE-CALCULATED DIRECTLY - AFTER ODE SOLUTION:

toc  %time counter end