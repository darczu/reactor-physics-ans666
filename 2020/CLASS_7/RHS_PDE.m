function dx = RHS_PDE_EXPLICIT(t,x,beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0)

dx = zeros(4,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(t < t0)
  rho_c = 0;    
elseif(t >=t0)
  rho_c = 0;    % Critical State - No Transient

% You can simulate different transients - to run the case uncomment
%
%   rho_c = +1.0*beta;                                % +1.0$ Reactivity Insertion - positive - i.e. Control Rod Ejection
%   rho_c = +0.1*beta;                                % +0.1$ 
%   rho_c = +0.3*beta;                                %
   rho_c = -5*beta;                                  % -5.0$ Negative Reactivity Insetion - i.e. Control Rods
%   T_cin = 545.0;                                     % Cold water transient - i.e. malfunction of Steam Generator 
%   T_cin = 555.0;                                     % Hot water transient - i.e. malfunction of Steam Generator 
%   w_c = (3/4)*17000.0 + (1/4)*17000*exp(-t/60.0);   % simulation of single Reactor Coolant Pump Trip 
%   rho_c = -1.2*beta*(t/60);                          % Ramp Control Rod insertion with insertion time equal 60s
%   elseif(t >=t0+60.0)                                   % Ramp Control Rod insertion with insertion time equal 60s
%   rho_c = -1.2*beta;                                % Ramp Control Rod insertion with insertion time equal 60s      
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reactivity given explicitly
% 21-01-2019 DURING THE CLASS I FORGOT TO ADD THIS PART:
rho = rho0 + rho_c + r_f*(x(3)-T_f0) + r_c*(x(4)-T_c0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dx(1) = ((rho-beta)/LAMBDA)*x(1) + (lambda)*x(2);
dx(2) = (beta/LAMBDA)*x(1) - (lambda)*x(2); 
dx(3) = (x(1) - hA*(x(3)-x(4)))/(m_f*cp_f);
dx(4) = (hA*(x(3)-x(4)) - 2*w_c*cp_c*(x(4)-T_cin))/(m_c*cp_c);

% x(1) - Power
% x(2) - Prec. Concentration
% x(3) - Fuel Temp
% x(4) - Mod Temp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%