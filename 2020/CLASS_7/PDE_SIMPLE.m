clear; close all; clc; format short g;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE VERSION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reactor Physics Tutorials  Rev 21-01-2019
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXECRCISE 1 CLASS 7 - POINT REACTOR DYNAMICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example based on Lecture 10 example
% Transient for Light Water Reactor. Use point neutron dynamics with 
% one neutron precursor group approximation to simulate some events/transients. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Reactor data:
LAMBDA = 1.0e-3;            %[s] neutron generation time
beta = 7.5e-3;              %[-] effective delayer neutron fraction
lambda = 0.08;              %[1/s] decay constant for neutron precursors
cp_c = 4000.0;              %[J/K/kg] spec. heat capacity for coolant
cp_f = 200.0;               %[J/kg/K] specific heat capacity for fuel 
m_f = 90000.0;              %[kg] fuel mass
m_c = 15000.0;              %[kg] coolant mass
w_c = 17000.0;              %[kg/s] coolant flow
T_cin = 550.0;              %[K] coolant inlet temperature
hA = 10.0e6;                %[W/K] Heat Transfer Coefficient x Heat Transfer Area
r_f = -2.0e-5;              %[1/K]  Doppler/Prompt Reactivity Feedback Coefficient 
r_c = -5.0e-5;              %[1/K]  Moderator/Delayed Reactivity Feedback Coefficient

t0 = 10.0;                   %[s] - transient initiation time 
TIME = [0.0, 100.0];        %[s] - initial and final time - time span

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Initial conditions
Power0 = 3411.0e6; 			%[Wth] Hot Full Power, Nominal Power
y_eq = beta*Power0/(LAMBDA*lambda);    %Precursors concentration at steady-state
rho0 = 0.0;                            %Critical core - initial reactivity 
T_c0 = T_cin + Power0/(2*w_c*cp_c);    %Steady-State coolant average temperature
T_f0 = T_c0 + Power0/(hA);             %Steady-State coolant average temperature
IC = [Power0, y_eq, T_f0, T_c0];       %Initial conditions vector

%% (a) Full power rapid control rod ejection. Rod worth is  0.4$.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SOLUTION
% Stiff solver
[T X] = ode23s(@(t,x) RHS_PDE_EXPLICIT(t,x,beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0), TIME, IC );
% Normal solver
%[T X] = ode23s(@(t,x) RHS_PDE_EXPLICIT(t,x,beta,LAMBDA,lambda,t0,rho0,cp_c,cp_f,m_f,m_c,w_c,T_cin,hA,r_f,r_c,T_c0,T_f0), TIME, IC );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLOTS 
figure(1)
plot(T,X(:,1)./1e6,'-r','LineWidth',2);
xlabel('Time, [s]'); ylabel('Power, [MWth]');
grid on;

figure(2)
plot(T,X(:,3),'-r','LineWidth',2);
ylabel('Fuel Temperature, [K]')
grid on;

figure(3)
plot(T,X(:,4),'-b','LineWidth',2);
xlabel('Time, [s]'); ylabel('Coolant Temperature, [K]');
grid on;

figure(4)
plot(T,X(:,2),'-b','LineWidth',2);
xlabel('Time, [s]'); ylabel('Precursor Concentration');
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



