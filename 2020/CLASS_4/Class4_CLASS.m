clear; clc; close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 4 - Reactor Physics Tutorials  Rev 06-01-2020
% Example 3 - The one-speed diffusion model for a homogenous critical PWR reactor
% 
% Use one-speed diffusion model of a bare, homogenous cylindrical reactor with material 
% composition representative of a modern PWR. We will use „homogenized” number densities corresponding 
% to a PWR core operating at full power conditions and containing a concentration of 2210 ppm of natural 
% boron (as boric acid) dissolved in the water coolant for control purposes. The fuel is taken as UO2 enriched to 2.78
% U235. In Table 5-2 we have listed the number densities and microscopic XS for this core. Data was used to calculate
% the macroscopic XS tabulated in Table 5-3. Transport XS used in this example have been artificially adjusted to take 
% some account of fast neutron leakage. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate:
% 1 - Diffusion coefficient
% 2 - Infinite multiplication factor 
% 3 - Material buckling
% 4 - Extrapolation distance
% 5 - Leakage fraction for a critical core.
% 6 - Critical core dimensions, assume core height 370 cm.
% PWR core similar to BEAVRS (example is based on example in Duderstadt page 210)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INPUT DATA:
run NUCLEAR_DATA_2019   %run script with nuclear data, micro XS, etc. In newer MATLAB add *.m
%Number densities [1/barn*cm]
N_H    = 2.748e-2;
N_O    = 2.757e-2;
N_ZR   = 3.694e-3;
N_FE   = 1.710e-3;
N_U235 = 1.909e-4;
N_U238 = 6.592e-3;
N_XE   = 0.0; %(1.0e15)*1e-24;    %for typical PWR (1.8e15)*1e-24 
%for typical PWR Xenon equilibrium concetration is (1.8e15)*1e-24  at/barn-cm
N_B    = 1.001e-5;  %Boron

%Macroscopic XSs for transport:
Sigma_tr_H    =  N_H   *sHtr;
Sigma_tr_O    =  N_O   *sOtr;
Sigma_tr_ZR   =  N_ZR  *sZRtr; 
Sigma_tr_FE   =  N_FE  *sFEtr;
Sigma_tr_U235 =  N_U235*sU235tr;
Sigma_tr_U238 =  N_U238*sU238tr;
Sigma_tr_B    =  N_B   *sB10tr;
Sigma_tr_XE   = N_XE   *sXE135tr;
Sigma_tr_TOT  = Sigma_tr_H+Sigma_tr_O+Sigma_tr_ZR+Sigma_tr_FE+...
                Sigma_tr_U235+Sigma_tr_U238 + Sigma_tr_B + Sigma_tr_XE;
            
%Macroscopic XSs for absorbtion
Sigma_a_H    =  N_H   *sHa;
Sigma_a_O    =  N_O   *sOa;
Sigma_a_ZR   =  N_ZR  *sZRa; 
Sigma_a_FE   =  N_FE  *sFEa;
Sigma_a_U235 =  N_U235*sU235a;
Sigma_a_U238 =  N_U238*sU238a;
Sigma_a_B    =  N_B   *sB10a;
Sigma_a_XE   =  N_XE  *sXE135a;
Sigma_a_TOT  = Sigma_a_H+Sigma_a_O+Sigma_a_ZR+Sigma_a_FE+...
               Sigma_a_U235+Sigma_a_U238+Sigma_a_B +Sigma_a_XE;
			   
%Macroscopic XSs for fission x nu - av. number of neutrons per fission
Sigma_nuf_U235 =  N_U235*sU235f*nuU235;
Sigma_nuf_U238 =  N_U238*sU238f*nuU238;
Sigma_nuf_TOT  =  Sigma_nuf_U235+Sigma_nuf_U238;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% SOLUTION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1 - Diffusion coefficient
lambda_tr = 1/Sigma_tr_TOT
D = lambda_tr/3 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2 - Infinite multiplication constant Estimation
k_inf = Sigma_nuf_TOT/ Sigma_a_TOT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3 - Material buckling
L2 = D/Sigma_a_TOT
Bm2 = (k_inf - 1)/L2 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4 - Extrapolation distance
d = 0.7104*lambda_tr

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5 - Leakage fraction for a critical core.
P_NL = 1.0/k_inf
P_L = 1.0 - P_NL

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6 - Critical core dimensions, assume core height 370 cm.
H = 370;  %Heigh
H_ex = H + 2*d

R_ex = sqrt( (2.405^2)/(Bm2 - (pi/H_ex)^2 )  )

R = R_ex - d







% ~180 cm.  It is underestimation 
% it should be noted that this is somewhat smaller than the radius of 180
% cm for a typical PWR core. This ilustrates the limitation of sich a
% one-speed model for obtaining quantitative estimates in reactor analysis.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




