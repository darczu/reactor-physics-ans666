 clear; clc; close all;   format long;  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Warsaw University of Technology Institute of Heat Engineering - Piotr Darnowski
% Class 5/Example 4 - Reactor Physics Tutorials  Rev 20-01-2019
% 2-group eigenvalue problem for infinite multiplying medium 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example 4 - Infinite Medium Two-Group Neutron Balance for PWR assembly
% Detailed neutron transport (lattice physics) calculations were performed for a 2D 
% model of a PWR fuel assembly. Calculations were performer by special computer code
% using 44 groups of neutrons with homogenization option to collapse 44 groups to
% 2 groups (fast and thermal) with boundary 0.625 eV. The code results are: 
% set of two-group neutron diffusion constants (for the reactor core simulator
% and fuel cycle analysis) and k-infnite. Compare (check) obtained k-inf and group
% constants by solving 2-group neutron balance equations in infinite medium. 
% The code results are given in the Table.  Treat the assembly as a point (0D). 
% Express the difference in terms of reactivity (in [pcm]).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
k_inf_code = 1.0963093;				%k-inf obtained with reference code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Absorbtion = Total - Scattering (Transfer) 
Sigma_a1 = 8.90788529E-03;		%1st group Absorbtion
Sigma_a2 = 5.85988164E-02 ;		%2nd group Absorbtion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Absorbtion matrix
A = [Sigma_a1, 0.0; ... 
     0.0,   Sigma_a2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OutScattering XS 
Sigma_out1to2 = 1.802136E-02;
Sigma_out2to1 = 0.0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OutScattering matrix - there is no downscattering from 2->1
Sout = [Sigma_out1to2, 0.0 ; ...
			0.0,  Sigma_out2to1];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% InScattering XSs 
Sigma_in2to1 = 0.0;
Sigma_in1to2 = 1.802136E-02;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% InScattering matrix  - there is no downscattering from 2->1
Sin = [0.0, Sigma_in2to1; ...
       Sigma_in1to2, 0.0 ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fission spectrum 
chi = [1.0 ; 0.0]; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fission reaction macro XS time nu av. number of neutrons produced per fission
nufiss1 = 5.01007447E-03;
nufiss2 = 7.97062814E-02;
nufiss = [nufiss1, nufiss2 ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fission matrix
F = chi*nufiss;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Migration matrix   
M = A + Sout - Sin;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Eigenvalue problem solution 

 B = inv(M)*F;
% Matrix inversion - important during the classes we tried to calculate F matrix inversion and it was imposbile 
% becouse F matrix is SINGULAR https://en.wikipedia.org/wiki/Invertible_matrix

 %B = F/M;    %it will work! More popular matrix "divison" command - be carful it is not "normal division". 
 [phi,k] = eig(B)   %eig(B,'balance')  eig(B,'nobalance')

% x = A\b is computed differently than x = inv(A)*b and is recommended for solving systems of linear equations: Ax=b.
% x = A\B solves the system of linear equations A*x = B.
% x = B/A solves the system of linear equations x*A = B for x.

k_inf = k(2,2)
k_inf_code 

% Check solution 
% phi1 = phi(:,2)
% k_inf*phi1 - B*phi1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison with the code

rho_difference = ((k_inf - k_inf_code)/(k_inf*k_inf_code))*1e5


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %Difference in terms of reactivity
% (k2-k1)/(k1*k2) *1e5 in units of [pcm] = per-cent-millirho or per-cent-mille

% Analitical Solution
k_ana = (nufiss1 + nufiss2*(Sigma_in1to2/Sigma_a2))/(Sigma_a1+Sigma_in1to2)







